package com.example.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.entity.User;

public interface UserRepo extends JpaRepository<User, Long>{
	
	void deleteUserById(Long id);

	Optional<User>  findUserById(Long id);

}
