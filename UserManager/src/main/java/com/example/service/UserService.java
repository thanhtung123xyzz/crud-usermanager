package com.example.service;

import java.util.List;

import com.example.entity.User;

public interface UserService {
	
	User addUser(User user);
	
	User updateUser(Long id, User user);
	
	List<User> getAllUser();
	
	User findUserById(Long id);
	
	void deleteUser(Long id);

}
